-- CreateTable
CREATE TABLE "users" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "email" VARCHAR(255) NOT NULL,
    "email_verified_at" TIMESTAMPTZ NOT NULL,
    "password" VARCHAR(255) NOT NULL,
    "remember_token" VARCHAR(100) NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL,
    "status" BOOLEAN NOT NULL,
    "un_remedy" VARCHAR(88) NOT NULL,
    "pass_remedy" VARCHAR(255) NOT NULL,
    "fn_remedy" VARCHAR(88) NOT NULL,
    "ln_remedy" VARCHAR(88) NOT NULL,
    "sgn_remedy" VARCHAR(88) NOT NULL,
    "sgn_id_remedy" VARCHAR(88) NOT NULL,
    "user_type" VARCHAR(8) NOT NULL,
    "employee_remedy_id" VARCHAR(100) NOT NULL,
    "is_delegate_enabled" BOOLEAN NOT NULL,
    "module_problem" BOOLEAN NOT NULL,
    "problem_role_id" INTEGER NOT NULL,
    "nip" VARCHAR(20) NOT NULL,
    "last_login" TIMESTAMPTZ NOT NULL,
    "profile" VARCHAR(64) NOT NULL,
    "type" VARCHAR(64) NOT NULL,

    CONSTRAINT "users_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "pm_roles" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(64) NOT NULL,
    "status" BOOLEAN NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "pm_roles_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "role_users" (
    "id" SERIAL NOT NULL,
    "user_id" INTEGER NOT NULL,
    "role_id" INTEGER NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL,

    CONSTRAINT "role_users_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "pm_roles_features" (
    "id" SERIAL NOT NULL,
    "fetured_id" INTEGER NOT NULL,
    "role_id" INTEGER NOT NULL,
    "action_create" BOOLEAN NOT NULL,
    "action_read" BOOLEAN NOT NULL,
    "action_update" BOOLEAN NOT NULL,
    "action_delete" BOOLEAN NOT NULL,
    "cretion_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL,

    CONSTRAINT "pm_roles_features_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "employee_remedy" (
    "id" VARCHAR(100) NOT NULL,
    "nip" VARCHAR(88) NOT NULL,
    "full_name" VARCHAR(188) NOT NULL,
    "first_name" VARCHAR(88) NOT NULL,
    "last_name" VARCHAR(88) NOT NULL,
    "profile_status" VARCHAR(30) NOT NULL,
    "company" VARCHAR(88) NOT NULL,
    "organization" VARCHAR(88) NOT NULL,
    "site_country" VARCHAR(88) NOT NULL,
    "site_state" VARCHAR(88) NOT NULL,

    CONSTRAINT "employee_remedy_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "inbox" (
    "UpdatedInDB" TIMESTAMPTZ NOT NULL,
    "ReceivingDateTime" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "Text" TEXT NOT NULL,
    "SenderNumber" VARCHAR(188) NOT NULL,
    "Coding" VARCHAR(225) NOT NULL,
    "UDH" TEXT NOT NULL,
    "SMSCNumber" VARCHAR(188) NOT NULL,
    "Class" INTEGER NOT NULL,
    "TextDecoded" TEXT NOT NULL,
    "id" SERIAL NOT NULL,
    "RecipientID" TEXT NOT NULL,
    "Processed" BOOLEAN NOT NULL,
    "Status" INTEGER NOT NULL,
    "RemedyTiketId" VARCHAR(15) NOT NULL,
    "ProcessStatus" VARCHAR(25) NOT NULL,
    "FinishDateTime" TIMESTAMPTZ NOT NULL,
    "ActionTicketCreation" VARCHAR(18) NOT NULL,
    "TicketCreationDateTime" TIMESTAMPTZ NOT NULL,
    "TelegramSenderId" INTEGER NOT NULL,
    "ServiceCatalogId" INTEGER NOT NULL,
    "EmployeeSenderId" VARCHAR(18) NOT NULL,
    "EmployeeAssignedId" VARCHAR(18) NOT NULL,
    "EmployeeAssigneeGroupId" VARCHAR(18) NOT NULL,
    "IncidentOdDecode" TEXT NOT NULL,
    "IncidentStartEkslasiDate" TIMESTAMPTZ NOT NULL,
    "IncidentEndEkslasiDate" TIMESTAMPTZ NOT NULL,
    "ProcessStatusEkskalasiIncident" VARCHAR(25) NOT NULL,

    CONSTRAINT "inbox_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "support_group_assignee_remedy" (
    "request_id" VARCHAR(100) NOT NULL,
    "remedy_login_id" VARCHAR(188) NOT NULL,
    "first_name" VARCHAR(188) NOT NULL,
    "last_name" VARCHAR(188) NOT NULL,
    "full_name" VARCHAR(188) NOT NULL,
    "company" VARCHAR(188) NOT NULL,
    "organization" VARCHAR(188) NOT NULL,
    "support_group_name" VARCHAR(188) NOT NULL,
    "support_group_id" VARCHAR(15) NOT NULL,
    "employee_remedy_id" TEXT NOT NULL,

    CONSTRAINT "support_group_assignee_remedy_pkey" PRIMARY KEY ("request_id")
);

-- CreateIndex
CREATE UNIQUE INDEX "users_un_remedy_key" ON "users"("un_remedy");

-- CreateIndex
CREATE UNIQUE INDEX "users_employee_remedy_id_key" ON "users"("employee_remedy_id");

-- CreateIndex
CREATE UNIQUE INDEX "users_problem_role_id_key" ON "users"("problem_role_id");

-- CreateIndex
CREATE UNIQUE INDEX "role_users_user_id_key" ON "role_users"("user_id");

-- CreateIndex
CREATE UNIQUE INDEX "pm_roles_features_role_id_key" ON "pm_roles_features"("role_id");

-- CreateIndex
CREATE UNIQUE INDEX "support_group_assignee_remedy_employee_remedy_id_key" ON "support_group_assignee_remedy"("employee_remedy_id");

-- AddForeignKey
ALTER TABLE "users" ADD CONSTRAINT "users_employee_remedy_id_fkey" FOREIGN KEY ("employee_remedy_id") REFERENCES "employee_remedy"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "users" ADD CONSTRAINT "users_problem_role_id_fkey" FOREIGN KEY ("problem_role_id") REFERENCES "pm_roles"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "role_users" ADD CONSTRAINT "role_users_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "pm_roles_features" ADD CONSTRAINT "pm_roles_features_role_id_fkey" FOREIGN KEY ("role_id") REFERENCES "role_users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "support_group_assignee_remedy" ADD CONSTRAINT "support_group_assignee_remedy_employee_remedy_id_fkey" FOREIGN KEY ("employee_remedy_id") REFERENCES "employee_remedy"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
