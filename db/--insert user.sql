--insert user
INSERT INTO users (
    "id",
    "name",
    "email",
    "password",
    "remember_token",
    "created_at",
    "updated_at",
    "status",
    "un_remedy",
    "pass_remedy",
    "fn_remedy",
    "ln_remedy",
    "sgn_remedy",
    "sgn_id_remedy",
    "user_type",
    "employee_remedy_id",
    "is_delegate_enabled",
    "module_problem",
    "problem_role_id",
    "nip",
    "last_login",
    "profile",
    "type"
) OVERRIDING SYSTEM VALUE VALUES (
    DEFAULT,
    'OD COMMAND CENTER',
    'od.comcen@bankmandiri.co.id',
    '',
    '$2y$10$46YqBzjxmbijeFuEMW14uesPdwUwG8zra5tsO5/izAFdz6qJeUseW',
    'lyOtAAnk8kDhvYjDPRv6Eobm3bZvI6NKHBGCocXmdkqpDyVgjoxWj81Y8NVT',
    '13/01/2021 18:02:52',
    '25/10/2023 15:25:28',
    't',
    'od.commandcenter',
    'eyJpdiI6IlByVi9UOU9RZTRhWUdWZktyTVY1SkE9PSIsInZhbHVlIjoiT1d0NDk4Q0ZyNU5QS2hzUEVkdC9wUUwwbkZZM1RFb0Ewa2VZbFNtU1Nwcz0iLCJtYWMiOiI3ZmIzOWE3NGY0NGQ1ODBkMjEwYTAyMmFkZmIyYWYyM2FmNDhjNTYxNzM5Yjc2OTMwMDA5MmYyODZlNWQ5MzlmIn0=',
    'OD',
    'COMMAND CENTER',
    'IT OCD - COMMAND CENTER',
    'SGP000000003114',
    'AGENT',
    'PPL000000147205',
    'f',
    'f',
    '',
    '',
    '25/10/2023 15:25:28',
    'Command Center - OPR',
    NULL
);