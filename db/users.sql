CREATE TABLE users (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name VARCHAR(200),
    email VARCHAR(225),
    password VARCHAR(225),
    status VARCHAR(100),
    un_remedy VARCHAR(100),
    pass_remedy VARCHAR(225),
    fn_remedy VARCHAR(150),
    ln_remedy VARCHAR(150),
    sgn_remedy VARCHAR(100),
    sgn_id_remedy VARCHAR(100),
    user_type VARCHAR(100),
    employee_remedy_id VARCHAR(200),
    is_delegate_enabled VARCHAR(2),
    module_problem VARCHAR(2),
    problem_role_id VARCHAR(20),
    nip VARCHAR(20),
    last_login DATE,
    profile VARCHAR(100),
    type VARCHAR(50)
);



--insert user
INSERT INTO users (
    "id",
    "name",
    "email",
    "email_verified_at",
    "password",
    "remember_token",
    "created_at",
    "updated_at",
    "status",
    "un_remedy",
    "pass_remedy",
    "fn_remedy",
    "ln_remedy",
    "sgn_remedy",
    "sgn_id_remedy",
    "user_type",
    "employee_remedy_id",
    "is_delegate_enabled",
    "module_problem",
    "problem_role_id",
    "nip",
    "last_login",
    "profile",
    "type"
) OVERRIDING SYSTEM VALUE VALUES (
    DEFAULT,
    'OD COMMAND CENTER',
    'od.comcen@bankmandiri.co.id',
    NULL,
    '$2y$10$46YqBzjxmbijeFuEMW14uesPdwUwG8zra5tsO5/izAFdz6qJeUseW',
    'lyOtAAnk8kDhvYjDPRv6Eobm3bZvI6NKHBGCocXmdkqpDyVgjoxWj81Y8NVT',
    '13/01/2021 18:02:52',
    '25/10/2023 15:25:28',
    't',
    'od.commandcenter',
    'eyJpdiI6IlByVi9UOU9RZTRhWUdWZktyTVY1SkE9PSIsInZhbHVlIjoiT1d0NDk4Q0ZyNU5QS2hzUEVkdC9wUUwwbkZZM1RFb0Ewa2VZbFNtU1Nwcz0iLCJtYWMiOiI3ZmIzOWE3NGY0NGQ1ODBkMjEwYTAyMmFkZmIyYWYyM2FmNDhjNTYxNzM5Yjc2OTMwMDA5MmYyODZlNWQ5MzlmIn0=',
    'OD',
    'COMMAND CENTER',
    'IT OCD - COMMAND CENTER',
    'SGP000000003114',
    'AGENT',
    'PPL000000147205',
    'f',
    'f',
    NULL,
    NULL,
    '25/10/2023 15:25:28',
    'Command Center - OPR',
    NULL
);

ALTER TABLE users ALTER COLUMN id TYPE INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY;


ALTER TABLE table_name
ALTER COLUMN column_name [SET DATA] TYPE new_data_type;